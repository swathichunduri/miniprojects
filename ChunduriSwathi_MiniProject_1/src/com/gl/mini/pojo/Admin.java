package com.gl.mini.pojo;

public class Admin 
{
	//attributes of admin class
	private int adminid;
	private String adminname;
	private String password;
	//getters and setters of admin class attributes
	public int getAdminid() 
	{
		return adminid;
	}
	public void setAdminid(int adminid) 
	{
		this.adminid = adminid;
	}
	public String getAdminname() 
	{
		return adminname;
	}
	public void setAdminname(String adminname) 
	{
		this.adminname = adminname;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}

}
