package com.gl.mini.pojo;

public class Order 
{
	private int item_no;
	public int getItem_no() {
		return item_no;
	}
	public void setItem_no(int item_no) {
		this.item_no = item_no;
	}
	public int getOrder_no() {
		return order_no;
	}
	public void setOrder_no(int order_no) {
		this.order_no = order_no;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getOrdered_date() {
		return ordered_date;
	}
	public void setOrdered_date(String strdate) {
		this.ordered_date = strdate;
	}
	public int getOrdered_quantity() {
		return ordered_quantity;
	}
	public void setOrdered_quantity(int ordered_quantity) {
		this.ordered_quantity = ordered_quantity;
	}
	private int order_no;
	private int amount;
	private String ordered_date;
	private int price;
	private int ordered_quantity;
}
