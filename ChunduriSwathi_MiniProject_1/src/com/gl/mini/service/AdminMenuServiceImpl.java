package com.gl.mini.service;
import java.util.*;

import com.gl.mini.dao.AdminMenuImpl;
import com.gl.mini.pojo.Item;
import com.gl.mini.pojo.Order;
public class AdminMenuServiceImpl implements AdminMenuService
{
	static Scanner sc;
	AdminMenuImpl mdao;
	public AdminMenuServiceImpl()
	{
		sc=new Scanner(System.in);
		mdao=new AdminMenuImpl();
	}
	@Override
	public void additem() 
	{
		System.out.println("Enter number of items you want to add");
		int noOfItems=sc.nextInt();
		for(int index=0;index<noOfItems;index++)
		{
			Item iobj=new Item();
			System.out.println("Enter item id");
			iobj.setItemId(sc.nextInt());
			System.out.println("Enter item name");
			iobj.setItemName(sc.next());
			System.out.println("Enter price of the item");
			iobj.setPrice(sc.nextInt());
			mdao.addItemDetails(iobj);
		}
	}

	@Override
	public void deleteitem() 
	{
		System.out.println("Enter the item id you want to delete");
		int item_id=sc.nextInt();
		Item deleteItem=new Item();
		deleteItem.setItemId(item_id);
		mdao.deleteItemDetails(deleteItem);
	}

	@Override
	public void updateitem() 
	{
		System.out.println("Enter the item id you want to update");
		int item_id=sc.nextInt();
		System.out.println("Enter new price of the item");
		int new_price=sc.nextInt();
		Item updateItem=new Item();
		updateItem.setItemId(item_id);
		updateItem.setPrice(new_price);
		mdao.updateItemDetails(updateItem);
	}

	@Override
	public void searchitem() 
	{
		System.out.println("Enter the item id you are searching");
		int item_id=sc.nextInt();
		Item searchItem=new Item();
		searchItem.setItemId(item_id);
		mdao.searchItemDetails(searchItem);
	}

	@Override
	public void displayitem() 
	{
		List<Item> itemList=mdao.displayItemDetails();
		for(Item i1:itemList)
		{
			System.out.println("ID:"+i1.getItemId()+"---->"+i1.getItemName()+"---->Rs."+i1.getPrice());
		}
	}
	
	@Override
	public void todaySale()
	{
		System.out.println("Enter todays date in the given format(yyyy-mm-dd)");
		String todaysDate=sc.next();
		Order today=new Order();
		today.setOrdered_date(todaysDate);
		mdao.seeTodaysSale(today);
	}
	
	@Override
	public void MonthSale()
	{
		Order ord1=new Order();
		mdao.seeMonthSales(ord1);
	}

}
