package com.gl.mini.service;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import com.gl.mini.dao.UserMenuImpl;
import com.gl.mini.pojo.Item;
import com.gl.mini.pojo.Order;

public class UserMenuServiceImpl implements UserMenuService
{
	UserMenuImpl umdi;
	static Scanner sc;
	static Date ordered_date;
	public UserMenuServiceImpl()
	{
		sc=new Scanner(System.in);
		umdi=new UserMenuImpl();
	}

	public void seeMenu() 
	{
		List<Item> itemList=umdi.displayItemDetails();
		System.out.println("----------------------MENU-------------------");
		for(Item i1:itemList)
		{
			System.out.println("ID:"+i1.getItemId()+"---->"+i1.getItemName()+"---->"+i1.getPrice());
		}
	}
	public void giveOrder()
	{
		System.out.println("Enter number of items you want to select");
		int noOfItems=sc.nextInt();
		for(int index=0;index<noOfItems;index++)
		{
			System.out.println("Enter the item number");
			int item_no=sc.nextInt();
			LocalDate localdate=LocalDate.now();
			String date=localdate.toString();
			System.out.println("enter the order quantity");
			int order_quantity=sc.nextInt();
			Order item=new Order();
			item.setItem_no(item_no);
			item.setOrdered_quantity(order_quantity);
			item.setOrdered_date(date);
			umdi.giveOrder(item);
		}
	}

	public void seeBill() 
	{
		Order order=new Order();
		umdi.seeBillDetais(order);
	}

}

