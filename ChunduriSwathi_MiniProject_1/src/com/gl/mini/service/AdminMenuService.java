package com.gl.mini.service;

public interface AdminMenuService {
	public void additem();
	public void deleteitem();
	public void updateitem();
	public void searchitem();
	public void displayitem();
	public void todaySale();
	public void MonthSale();
}