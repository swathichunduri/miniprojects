package com.gl.mini;
import java.io.IOException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import com.gl.mini.service.AdminServiceImpl;
import com.gl.mini.service.UserMenuServiceImpl;
import com.gl.mini.exceptions.NegativeNumberException;
import com.gl.mini.service.AdminMenuServiceImpl;
import com.gl.mini.service.UserServiceImpl;
//Extending the Thread class.
public class App extends Thread
{
	static Scanner sc;
	AdminServiceImpl asi;
	UserServiceImpl usi;
	AdminMenuServiceImpl amsi;
	UserMenuServiceImpl umsi;
	public App()
	{
		sc=new Scanner(System.in);
		asi=new AdminServiceImpl();
		usi=new UserServiceImpl();
		amsi=new AdminMenuServiceImpl();
		umsi=new UserMenuServiceImpl();
	}
	//Thread class run method
	public void run()
	{
		int choice;
		do
		{
			System.out.println("1.Admin");
			System.out.println("2.User");
			System.out.println("0.logout");
			System.out.println("Enter your choice ");
			choice=sc.nextInt();
			switch(choice)
			{
			case 1:
				try 
				{
					adminFunctions();
				}
				catch(Exception ex)
				{
					System.out.println(ex.getMessage());
				}
				break;
			case 2:
				try 
				{
					userfunctions();
				} 
				catch (NegativeNumberException e) 
				{
					System.out.println(e.getMessage());
				}
			}
		}while(choice!=0);
	}
	//admin registration 
	public boolean adminMenu()
	{
		int choice;
		do
		{
			System.out.println("--------------WELOME TO ADMIN LOGIN-------------");
			System.out.println("1.Login");
			System.out.println("0.logout");
			System.out.println("Enter your choice ");
			choice=sc.nextInt();
			switch(choice)
			{
				case 1:
					return asi.adminLogin(true);
				default:
					System.out.println("Sorry check the option and enter again");
					break;
			}
		}while(choice!=0);
		return false;
	}
	//admin functions
	private void adminFunctions() throws NegativeNumberException
	{
		if(this.adminMenu())
		{
		int choice;
		do 
		{
			System.out.println("--------------------------ADMIN MENU-----------------------");
			System.out.println("1.Add items");
			System.out.println("2.Delete items");
			System.out.println("3.update items");
			System.out.println("4.search items");
			System.out.println("5.display all items");
			System.out.println("6.Total sales of today");
			System.out.println("7.Total sales of month");
			System.out.println("0.Exit");
			System.out.println("Enter your choice: ");
			choice=sc.nextInt();
			if(choice<0)
			{
				//throws negative number exception when user enter negative number.
				throw new NegativeNumberException();
			}
			switch(choice)
			{
			case 0:
					System.out.println("*************Thanks for Visiting*********************");
					break;
			case 1:
					amsi.additem();
					break;
			case 2:
					amsi.deleteitem();
					break;
			case 3:
					amsi.updateitem();
					break;
			case 4:
					amsi.searchitem();
					break;
			case 5:
					amsi.displayitem();
					break;
			case 6:
					amsi.todaySale();
					break;
			case 7:
					amsi.MonthSale();
					break;
			default:
				System.out.println("Sorry check the option and enter again");
				break;
			}
		}while(choice!=0);
		}
	}
	// user menu for the surabi restautant
	public boolean userMenu() 
	{
		int choice;
		do
		{
			System.out.println("----------------WELCOME TO REGISTRATON PAGE------------------");
			System.out.println("1.Register");
			System.out.println("2.Login");
			System.out.println("0.logout");
			System.out.println("Enter your choice ");
			choice=sc.nextInt();
			switch(choice)
			{
			case 1:
				usi.userRegister();
			case 2:
				return usi.UserLogin(true);
			default:
				System.out.println("Sorry check the option and enter again");
				break;
			}
		}while(choice!=0);
		return false;
	}
	//user functions 
	public void userfunctions() throws NegativeNumberException
	{
		if(this.userMenu())
		{
			int choice;
			do
			{
				System.out.println("----------------------USER MENU------------------------");
				System.out.println("1.See Menu");
				System.out.println("2.Order");
				System.out.println("3.Bill");
				System.out.println("0.Exit");
				System.out.println("Enter your choice");
				choice=sc.nextInt();
				if(choice<0)
				{
					//throws negative number exception when user enter negative number.
					throw new NegativeNumberException();
				}
				switch(choice)
				{
				case 0:
					 System.out.println("****************Thanks for Visiting*******************");
					 break;
				case 1:
					  umsi.seeMenu();
					  break;
				case 2:
					  umsi.giveOrder();
					  break;
				case 3:
					 umsi.seeBill();
					 break;
				default:
					System.out.println("Sorry check the option and enter again");
					break;
				}
			}while(choice!=0);
		}
	}
	public static void main(String[] args) 
	{
		//Excecution of loggers
		boolean append = true;
        FileHandler handler = null;
		try 
		{
			//stores the loggers in the given path.
			handler = new FileHandler("C:\\Users\\swathi.chunduri\\eclipse-workspace\\ChunduriSwathi_MiniProject_1\\src\\log.txt", append);
		} 
		catch (SecurityException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        logger.addHandler(handler);
        logger.severe("severe message");
        logger.warning("warning message");
        logger.info("info message");
        logger.config("config message");
        logger.fine("fine message");
        logger.finer("finer message");
        logger.finest("finest message");
        System.out.println("-----------------------------------------------------------------------");
		System.out.println("--------------------WELCOME TO SURABI RESTAURENT-----------------------");
		 System.out.println("-----------------------------------------------------------------------");
		App app=new App();
		//starting the thread
		app.start();
	}

}
