package com.gl.mini.dao;

import java.util.List;

import com.gl.mini.pojo.Item;
import com.gl.mini.pojo.Order;

public interface UserMenu 
{
	public void giveOrder(Order order);
	public List<Item> displayItemDetails();
	public void seeBillDetais(Order order);
}
