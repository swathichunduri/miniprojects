package com.gl.mini.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.gl.mini.connect.DataConnect;
import com.gl.mini.pojo.Item;
import com.gl.mini.pojo.Order;

public class UserMenuImpl implements UserMenu
{
	private Connection con1;
	private PreparedStatement stat=null;
	private int oldordid=0;
	private int totalbill=0;
	private int total=0;
	public UserMenuImpl()
	{
		con1=DataConnect.getConnect();
	}
	
	@Override
	public List<Item> displayItemDetails() 
	{
		List<Item> itemlist=new ArrayList<Item>();
		try 
		{
			//displays the items in the menu
			stat=con1.prepareStatement("select * from itemmenu");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Item item1=new Item();
				item1.setItemId(result.getInt(1));
				item1.setItemName(result.getString(2));
				item1.setPrice(result.getInt(3));
				itemlist.add(item1);
			}
			
		} 
		catch (SQLException e) 
		{

			e.printStackTrace();
		}
		return itemlist;
	}
	
	@Override
	public void giveOrder(Order order) 
	{
		try 
		{
			stat=con1.prepareStatement("select max(orderid) from orders");
			ResultSet oresult=stat.executeQuery();
			while(oresult.next())
			{
				oldordid=oresult.getInt(1);
			}
			int newordid=oldordid+1;
			String stmt="select itemprice from itemmenu  where itemid=?";
			stat=con1.prepareStatement(stmt);
			stat.setInt(1, order.getItem_no());
			ResultSet result=stat.executeQuery();
			int price=0;
			//iterates over the rows of table
			while(result.next())
			{
				//getInt() used to retrieve the value from current row 
			   price=result.getInt(1);
			   
			}
			total=price*(order.getOrdered_quantity());
			stat=con1.prepareStatement("insert into orders values(?,?,?,?,?,?)");
			stat.setInt(1, newordid);
			stat.setString(2, order.getOrdered_date());
			stat.setInt(3, order.getOrdered_quantity());
			stat.setInt(4,price);
			stat.setInt(5,total);
			stat.setInt(6,order.getItem_no());
			try {
			int res=stat.executeUpdate();
			if(res>0)
			{
				System.out.println("Orders taken sucessfully");
			}
			totalbill=totalbill+total;
			}
			catch(Exception ex)
			{
				System.out.println("Sorry,item not found at this moment");
			}			
		}
			
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void seeBillDetais(Order order) 
	{
		System.out.println("----------------TOTAL BILL------------------");
		System.out.println("\t\t\tRs."+totalbill);
	}

	
}
