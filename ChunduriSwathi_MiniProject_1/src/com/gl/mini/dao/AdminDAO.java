package com.gl.mini.dao;

import java.util.List;

import com.gl.mini.pojo.Admin;

public interface AdminDAO 
{
	public List<Admin> login();
}
