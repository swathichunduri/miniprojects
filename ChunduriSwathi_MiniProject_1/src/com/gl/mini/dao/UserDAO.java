package com.gl.mini.dao;

import java.util.*;

import com.gl.mini.pojo.User;

public interface UserDAO {
	public void register(User user);
	public List<User> login();
}
