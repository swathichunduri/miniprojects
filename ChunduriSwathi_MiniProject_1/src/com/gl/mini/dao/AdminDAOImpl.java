package com.gl.mini.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.gl.mini.connect.DataConnect;
import com.gl.mini.pojo.Admin;

public class AdminDAOImpl implements AdminDAO{
	static Scanner sc;
	private Connection con1;
	private PreparedStatement stat;
	public AdminDAOImpl()
	{
		//connects to the database.
		con1=DataConnect.getConnect();
	}
	@Override
	public List<Admin> login()
	{
		List<Admin> adminlist=new ArrayList<Admin>();
		try
		{
			stat=con1.prepareStatement("select adminname,adminpassword from admin");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Admin a1=new Admin();
				a1.setAdminname(result.getString(1));
				a1.setPassword(result.getString(2));
				adminlist.add(a1);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return adminlist;
	}
}
