package com.gl.mini.dao;

import java.util.*;

import com.gl.mini.pojo.Item;

public interface AdminMenu 
{
	//list of admin functions.
	public void addItemDetails(Item iobj);
	public void deleteItemDetails(Item deleteitem);
	public void updateItemDetails(Item updateItem);
	public void searchItemDetails(Item searchItem);
	public List<Item> displayItemDetails() ;
}
