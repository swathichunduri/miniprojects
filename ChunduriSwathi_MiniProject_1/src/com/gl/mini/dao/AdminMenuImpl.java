package com.gl.mini.dao;
import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.gl.mini.connect.DataConnect;
import com.gl.mini.pojo.Item;
import com.gl.mini.pojo.Order;

public class AdminMenuImpl implements AdminMenu
{
	Scanner sc=new Scanner(System.in);
	private Connection con1;
	private PreparedStatement stat;
	public AdminMenuImpl()
	{
		//connects to the database.
		con1=DataConnect.getConnect();
	}
	public void addItemDetails(Item iobj) 
	{
		try
		{
			//inserts values into itemmenu database.
			stat=con1.prepareStatement("insert into itemmenu values(?,?,?)");
			stat.setInt(1, iobj.getItemId());
			stat.setString(2, iobj.getItemName());
			stat.setInt(3, iobj.getPrice());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Item added successfully");
			}
		}
		catch(SQLException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	public void deleteItemDetails(Item deleteitem) 
	{
		try 
		{
			//deletes the item fron the database.
			stat=con1.prepareStatement("delete from itemmenu where itemid=?");
			stat.setInt(1, deleteitem.getItemId());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Item deleted sucessfully");
			}
			else
			{
				System.out.println("No item found with that itemId");
			}
		} 
		catch (SQLException e) 
		{
			System.out.println(e.getMessage());
		}
	}

	public void updateItemDetails(Item updateItem) 
	{
		try 
		{
			//updates the value of price in the database.
			stat=con1.prepareStatement("update itemmenu set itemprice=? where itemid=?");
			stat.setInt(1, updateItem.getPrice());
			stat.setInt(2, updateItem.getItemId());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Item updated successfully.");
			}
			else
			{
				System.out.println("Sorry,item not found.");
			}
		} 
		catch (SQLException e) 
		{
			System.out.println(e.getMessage());
		}
	}

	public void searchItemDetails(Item searchItem) 
	{
		try 
		{
			//searches the item from the table using itemid.
			stat=con1.prepareStatement("select * from itemmenu where itemid=?");
			stat.setInt(1, searchItem.getItemId());
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				System.out.println("Item found sucessfully");
			}
		}
		catch (SQLException e) 
		{
			System.out.println(e.getMessage());
		}
		
	}

	public List<Item> displayItemDetails() 
	{
		List<Item> itemlist=new ArrayList<Item>();
		try 
		{
			//displays all the items along with price
			stat=con1.prepareStatement("select * from itemmenu");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Item item1=new Item();
				item1.setItemId(result.getInt(1));
				item1.setItemName(result.getString(2));
				item1.setPrice(result.getInt(3));
				itemlist.add(item1);
			}
			
		} 
		catch (SQLException e) 
		{

			e.printStackTrace();
		}
		return itemlist;
		
	}
	public void seeTodaysSale(Order sale) 
	{
		try 
		{
			//shows the total amount by giving the date.
			stat=con1.prepareStatement("select sum(totalamount) from orders where orderdate=?");
			stat.setString(1,sale.getOrdered_date());
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				int amt=result.getInt(1);
				System.out.println("\t\tSALES ON "+sale.getOrdered_date());
				System.out.println("---------------------------------------------------");
				System.out.println("\t\t\tRs."+amt);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void seeMonthSales(Order monthSale)
	{
		try
		{
			//can add amount from two different dates.
			System.out.println("start date of the month in this format(yyyy-mm-dd)");
			String from=sc.next();
			System.out.println("Enter end date of the month in this format(yyyy-mm-dd)");
			String to=sc.next();
			stat=con1.prepareStatement("select sum(totalamount) from orders where orderdate between ? and ?" );
			stat.setString(1, from);
			stat.setString(2, to);
			ResultSet result=stat.executeQuery();
			int total=0;
			while(result.next())
			{
				
				total=total+result.getInt(1);
				System.out.println("\t\tSALES BETWEEN "+from+" AND "+to);
				System.out.println("-------------------------------------------------");
				System.out.println("\t\t\tRs."+total);
				
			}
		}
		catch(SQLException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

}
