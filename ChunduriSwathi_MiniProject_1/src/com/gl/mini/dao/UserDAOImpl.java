package com.gl.mini.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.*;
import com.gl.mini.connect.DataConnect;
import com.gl.mini.pojo.User;

public class UserDAOImpl implements UserDAO{

	static Scanner sc;
	private Connection con1;
	private PreparedStatement stat;
	public UserDAOImpl()
	{
		con1=DataConnect.getConnect();
	}
	@Override
	public void register(User user) 
	{
		try
		{
			//inserts data into user table
			stat=con1.prepareStatement("insert into user values(?,?,?)");
			stat.setInt(1,user.getUserid());
			stat.setString(2, user.getUsername());
			stat.setString(3, user.getPassword());
			int result=stat.executeUpdate();
			//checks the data inserted sucessfully are not
			if(result>0)
			{
				System.out.println("Data inserted");
			}
		}
		catch(SQLException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public List<User> login()
	{
		List<User> userlist=new ArrayList<User>();
		try
		{
			stat=con1.prepareStatement("select username,password from user");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				User u1=new User();
				u1.setUsername(result.getString(1));
				u1.setPassword(result.getString(2));
				userlist.add(u1);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return userlist;
	}

}