/*creating surabi restaurants database*/
create database restaurants;

/*usage of database*/
use surabi_restaurants;

/*creating table user*/
create table user
(
	userid int primary key,
	username varchar(30),
	password varchar(30)
);

/*create table admin*/
create table admin
(
	adminid int primary key,
	adminname varchar(30),
	adminpassword varchar(30)
);

/*create table menu*/
create table itemmenu
(
	itemid int primary key,
	itemname varchar(30),
	itemprice int
);

/*creating orders table*/
create table orders
(
	orderid int,
    orderdate varchar(20),
    orderquantity int,
    price int,
    totalamount int,
    itemid int
);
/*inserting admin into database*/
insert into admin values(1,"admin","admin");

/*checking description*/
desc orders;

select sum(totalamount) from orders where orderdate between ? and ?;


select sum(totalamount) from orders where orderdate=?;

/*selecting itemsmenu*/
select * from itemmenu;