<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
	<h1 style="color: Green">SURABI RESTAURANTS</h1>
		<h2 style="color: Orange">ORDER FORM</h2>
		<form:form method="POST" action="SaveOrder" modelAttribute="order">
			<table>
				<tr><td><form:hidden  path="itemid" /></td>  </tr>
				<tr><td><form:hidden  path="itemname" /></td>  </tr>
				<tr><td><form:hidden  path="price" /></td>  </tr>
				<tr><td><form:hidden  path="date" /></td>  </tr>
				<tr><td><form:hidden  path="totalPrice" /></td>
				 </tr>
				<tr>
					<td><form:label path="quantity">Enter how many plates :</form:label></td>
					<td><form:input path="quantity" /></td>
				</tr>
				<tr>
					<td><form:label path="username">Please Enter your Name:</form:label></td>
					<td><form:input path="username" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit" value="ADD" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>