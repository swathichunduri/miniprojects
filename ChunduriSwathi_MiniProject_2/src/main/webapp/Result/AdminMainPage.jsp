<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">WELCOME TO SURABI RESTAURANTS.</h1>
		<h2 style="color: Orange">DASHBOARD</h2>
		<h3>
			<a href="RetrieveUsers">User Operations</a>
		</h3>
		<h3>
			<a href="RetrieveItems">Menu Operations</a>
		</h3>
		<h3>
			<a href="SeeTodaysBill">Today's Bill</a>
		</h3>
		<h3>
			<a href="SeeMonthBill">Month's Bill</a>
		</h3>
		<h3>
			<a href="OrderbySpecificUser">Orders by Specific User</a>
		</h3>
		<br> <a href="logout">LOGOUT</a>
	</div>
</body>
</html>