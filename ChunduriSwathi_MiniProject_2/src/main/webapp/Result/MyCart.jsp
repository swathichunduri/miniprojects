<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">SURABI RESTAURANTS</h1>
		<h2 style="color: Orange">MY CART</h2>
		<h3>
			<a href="DisplayMenu">DashBoard</a>
		</h3>
		<table border="1">
			<th style="color:red">SNO</th>
			<th style="color:red">CART ID</th>
			<th style="color:red">ITEM ID</th>
			<th style="color:red">ITEM NAME</th>
			<th style="color:red">PRICE</th>

			<c:forEach var="item" items="${cartlist}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${item.cartid}</td>
					<td>${item.itemid}</td>
					<td>${item.itemname}</td>
					<td>${item.price}</td>
				</tr>
			</c:forEach>
		</table>
		<a href="logout">LOGOUT</a>
	</div>
</body>
</html>