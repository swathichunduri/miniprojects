<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Surabi Restaurants</title>
</head>
<body>
	<div align="center">
	<h1 style="color: Green">SURABI RESTAURANTS</h1>
		<h2 style="color: Orange">USER LOGIN FORM</h2>
			<form:form method="POST" action="UserLoginValidation"
			modelAttribute="user2">
			<table>
				<tr>
					<td><form:label path="userName">User Name</form:label></td>
					<td><form:input path="userName" /></td>
				</tr>
				<tr>
					<td><form:label path="password">Password</form:label></td>
					<td><form:input path="password" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit" value="Login" /></td>
				</tr>
			</table>
			<p>If you don't have an account here<br><br>
				Click here to create an account<br><br> <a
					href="userRegistrationLink">Register here</a>
			</p>
		</form:form>
	</div>
</body>
</html>