<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">SURABI RESTAURANTS</h1>
		<h2 style="color: Orange">USER INSERT FORM</h2>
		<form:form method="POST" action="addUser" modelAttribute="user3">
			<table>
				<tr>
					<td><form:label path="userName">User Name</form:label></td>
					<td><form:input path="userName" /></td>
				</tr>
				<tr>
					<td><form:label path="password">Password</form:label></td>
					<td><form:input path="password" /></td>
				</tr>
				<tr>
					<td><form:label path="emailId">Email Id</form:label></td>
					<td><form:input path="emailId" /></td>
				</tr>
				<tr>
					<td><form:label path="phoneNumber">Phone Number</form:label></td>
					<td><form:input path="phoneNumber" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="ADD" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>