<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
		<h1 style="color:Green">SURABI RESTAURANTS</h1>
		<h3 style="color:Orange">MY ORDER HISTORY</h3>
		<h3><a href="DisplayMenu">DashBoard</a></h3>
		<table border="1">
			<th style="color:red">SNO</th>
			<th style="color:red">DATE</th>
			<th style="color:red">ORDER ID</th>
			<th style="color:red">ITEM ID</th>
			<th style="color:red">ITEM NAME</th>
			<th style="color:red">PRICE</th>
			<th style="color:red">QUANTITY</th>

			<c:forEach var="item" items="${orderlist}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${item.date}</td>
					<td>${item.orderId}</td>
					<td>${item.itemid}</td>
					<td>${item.itemname}</td>
					<td>${item.price}</td>
					<td>${item.quantity}</td>
				</tr>
			</c:forEach>
		</table>
		<a href="logout">LOGOUT</a>
	</div>
</body>
</html>