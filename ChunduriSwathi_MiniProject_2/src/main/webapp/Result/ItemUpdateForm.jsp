<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">SURABI RESTAURANTS</h1>
		<h2 style="color: Orange">ITEM UPDATE FORM</h2>
		<form:form method="POST" action="getItems" ModelAttribute="command">
			<table>
				<tr>
					<td><form:label path="itemName">Item Name</form:label></td>
					<td><form:input path="itemName" /></td>
				</tr>
				<tr>
					<td><form:label path="price">Price</form:label></td>
					<td><form:input path="price" /></td>
				</tr>
				<tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="Update" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>