<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">SURABI RESTAURANTS</h1>
		<h2 style="color: Orange">USERS</h2>
		<h3>
			<a href="AdminDashBoard">DashBoard</a>
		</h3>
		<h3>
			<a href="newUser">ADD USER</a>
		</h3>
		<table border="1">
			<th style="color: red">SNO</th>
			<th style="color: red">USER NAME</th>
			<th style="color: red">PASSWORD</th>
			<th style="color: red">PHONE NUMBER</th>
			<th style="color: red">EMAILID</th>
			<th style="color: red">ACTION</th>

			<c:forEach var="user" items="${listUser}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${user.userName}</td>
					<td>${user.password}</td>
					<td>${user.phoneNumber}</td>
					<td>${user.emailId}</td>
					<td><a href="editUser?userId=${user.userId}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="deleteUser?userId=${user.userId}">Delete</a></td>

				</tr>
			</c:forEach>
		</table>
		<a href="logout">LOGOUT</a>
	</div>
</body>
</html>