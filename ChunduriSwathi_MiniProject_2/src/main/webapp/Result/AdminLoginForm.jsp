<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SURABI RESTAURANTS</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">SURABI RESTAURANTS</h1>
		<h2 style="color: Orange">ADMIN LOGIN FORM</h2>
		<form:form method="POST" action="CheckAdminLogin"
			modelAttribute="admin1">
			<table>
				<tr>
					<td><form:label path="adminName">Enter Admin Name : </form:label></td>
					<td><form:input path="adminName" /></td>
				</tr>
				<tr>
					<td><form:label path="password">Enter Password : </form:label></td>
					<td><form:input path="password" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="Login" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>