<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Surabi Restaurants</title>
</head>
<body>
	<div align="center">
		<h1 style="color: green">WELCOME TO SURABI RESTURANTS</h1>
		<h3 style="color: Orange">DashBoard</h3>
		<table border="1">
			<th style="color:red">SNO</th>
			<th style="color:red">ITEM ID</th>
			<th style="color:red">ITEM NAME</th>
			<th style="color:red">PRICE</th>
			<th style="color:red">ACTION</th>

			<c:forEach var="item" items="${itemList}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${item.itemId}</td>
					<td>${item.itemName}</td>
					<td>${item.price}</td>
					<td><a href="AddCart?itemId=${item.itemId}">AddToCart</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="PlaceOrder?itemId=${item.itemId}">PlaceOrder</a></td>
				</tr>
			</c:forEach>
		</table>
		<h3>
			<a href="SesMyCart">MyCart</a>
		</h3>
		<h3>
			<a href="OrderHistory">MyOrderHistory</a>
		</h3>
		<a href="logout">LOGOUT</a>
	</div>
</body>
</html>