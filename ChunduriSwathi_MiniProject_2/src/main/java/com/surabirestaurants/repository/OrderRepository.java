package com.surabirestaurants.repository;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.surabirestaurants.entity.Order;
import com.surabirestaurants.entity.User;

@Repository
public interface OrderRepository extends JpaRepository<Order,Integer>
{
	//List order  by using user name..
	public List<Order> findAllByUsername(String username);
	
	//list order by using user name and date
	public List<Order> findAllByUsernameOrderByDateDesc(String username);
	
	//order list using date
	public List<Order> findAllByDate(LocalDate date);
	
	//adds the total price having given user name and date
	@Query("select sum(ord.totalPrice) from Order ord where ord.date=?1 and ord.username=?2")
	double getBill(LocalDate date, String user);
	
	//adds the total price based on the date
	@Query("select sum(ord.totalPrice) from Order ord where ord.date=?1")
	public double getTodaysbill(LocalDate date);
	
	//adds the total price based on the month 
	@Query("select sum(ord.totalPrice) from Order ord where month(ord.date)=?1")
	public double getMonthSale(int month);
	
}
