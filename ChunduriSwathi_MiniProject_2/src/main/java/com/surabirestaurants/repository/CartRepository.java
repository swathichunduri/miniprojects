package com.surabirestaurants.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.surabirestaurants.entity.Cart;
@Repository
public interface CartRepository extends JpaRepository<Cart,Integer>
{
	//finds list of cart by using user name
	public List<Cart> findByUsername(String username);
	
}
