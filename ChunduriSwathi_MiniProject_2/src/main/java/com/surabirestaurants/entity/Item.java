package com.surabirestaurants.entity;

import javax.persistence.*;

@Entity
@Table(name="items")//table name
public class Item 
{
	//attributes of item class
	@Id//primary key
	@GeneratedValue
	private int itemId;
	private String itemName;
	private int price;
	public int getItemId() 
	{
		return itemId;
	}
	public void setItemId(int itemId)
	{
		this.itemId = itemId;
	}
	public String getItemName() 
	{
		return itemName;
	}
	public void setItemName(String itemName) 
	{
		this.itemName = itemName;
	}
	public int getPrice() 
	{
		return price;
	}
	public void setPrice(int price) 
	{
		this.price = price;
	}
}
