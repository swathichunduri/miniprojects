package com.surabirestaurants.entity;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="orders")//table name
public class Order 
{
	//attributes of order class
	@Id//primary key
	@GeneratedValue
	private int orderId;
	private LocalDate date;
	private int itemid;
	private String itemname;
	private int quantity;
	private int price;
	private double totalPrice;
	private String username;
	//getters and setters of attributes present in order class
	public String getUsername() 
	{
		return username;
	}
	public void setUsername(String username) 
	{
		this.username = username;
	}
	public int getOrderId()
	{
		return orderId;
	}
	public void setOrderId(int orderId) 
	{
		this.orderId = orderId;
	}
	public LocalDate getDate()
	{
		return date;
	}
	public void setDate(LocalDate date) 
	{
		this.date = date;
	}
	public int getItemid()
	{
		return itemid;
	}
	public void setItemid(int itemid)
	{
		this.itemid = itemid;
	}
	public String getItemname()
	{
		return itemname;
	}
	public void setItemname(String itemname) 
	{
		this.itemname = itemname;
	}
	public int getPrice()
	{
		return price;
	}
	public void setPrice(int price) 
	{
		this.price = price;
	}
	public int getQuantity() 
	{
		return quantity;
	}
	public void setQuantity(int quantity) 
	{
		this.quantity = quantity;
	}
	public double getTotalPrice() 
	{
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) 
	{
		this.totalPrice = totalPrice;
	}
}
