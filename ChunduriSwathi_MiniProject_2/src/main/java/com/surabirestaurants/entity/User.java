package com.surabirestaurants.entity;

import javax.persistence.*;

@Entity
@Table(name="users")//table name
public class User 
{
	//attributes of user class
	@Id//primary key
	@GeneratedValue
	private int userId;
	private String userName;
	private String password;
	private String emailId;
	private String phoneNumber;
	//getters and setters of the user class..
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId) 
	{
		this.userId = userId;
	}
	public String getUserName() 
	{
		return userName;
	}
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}
	public String getEmailId()
	{
		return emailId;
	}
	public void setEmailId(String emailId) 
	{
		this.emailId = emailId;
	}
	public String getPhoneNumber() 
	{
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}
}
