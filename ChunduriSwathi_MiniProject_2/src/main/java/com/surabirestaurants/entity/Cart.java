package com.surabirestaurants.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cart")//table name 
public class Cart
{
	//attributes of cart class
	@Id//primary key
	@GeneratedValue
	private int cartid;
	private int itemid;
	private String itemname;
	private int price;
	private String username;
	public int getCartid()
	{
		return cartid;
	}
	public void setCartid(int cartid)
	{
		this.cartid = cartid;
	}
	public int getItemid() 
	{
		return itemid;
	}
	public void setItemid(int itemid) 
	{
		this.itemid = itemid;
	}
	public String getItemname() 
	{
		return itemname;
	}
	public void setItemname(String itemname) 
	{
		this.itemname = itemname;
	}
	public int getPrice() 
	{
		return price;
	}
	public void setPrice(int price) 
	{
		this.price = price;
	}
	public String getUsername() 
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
}
