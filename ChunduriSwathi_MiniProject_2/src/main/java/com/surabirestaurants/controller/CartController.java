package com.surabirestaurants.controller;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.surabirestaurants.entity.Cart;
import com.surabirestaurants.entity.Item;
import com.surabirestaurants.repository.CartRepository;
import com.surabirestaurants.repository.ItemRepository;

@Controller
public class CartController 
{
	@Autowired 
	ItemRepository irep;
	@Autowired 
	CartRepository crep;
	
	//In this the cart items will be added to the database...
	@RequestMapping("/AddCart")
	public ModelAndView getform(HttpServletRequest request)
	{
		int item_id=Integer.parseInt(request.getParameter("itemId"));
		Optional <Item> itemlist=irep.findById(item_id);
		Item i=itemlist.get();
		Cart cart=new Cart();
		cart.setItemid(i.getItemId());
		cart.setItemname(i.getItemName());
		cart.setPrice(i.getPrice());
		cart.setUsername(cart.getUsername());
		return new ModelAndView("CartForm","cart",cart);
	}
	
	@RequestMapping("/SaveToCart")
	public String saveToCart(@ModelAttribute ("cart") Cart c1)
	{
		crep.save(c1);
		return "redirect:/DisplayMenu";
	}
	
	//Here we can see the items added to the cart   
	@RequestMapping("/SesMyCart")
	public ModelAndView seeCart()
	{
		return new ModelAndView("UserCartForm","cart1",new Cart());
	}
	
	@RequestMapping("/ShowCart")
	public ModelAndView seeItems(@ModelAttribute("cart1") Cart c)
	{
		List<Cart> username=crep.findByUsername(c.getUsername());
		return new ModelAndView("MyCart","cartlist",username);
	}
}
