package com.surabirestaurants.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.surabirestaurants.entity.Item;
import com.surabirestaurants.repository.ItemRepository;

@Controller
public class ItemCrudController 
{
	Item ditem;
	@Autowired
	ItemRepository irep;
	
	@RequestMapping("/newItem")
	public ModelAndView getInsertForm()
	{
		//form will be displayed to add the items ..
		return new ModelAndView("ItemInsertForm","item1",new Item());
	}
	@RequestMapping("/addItem")
	public String addingItem(@ModelAttribute ("item1") Item item)
	{
		//directly adds the items into the database using save() function..
		irep.save(item);
		return "redirect:/RetrieveItems";
	}
	
	@RequestMapping("/RetrieveItems")
	public ModelAndView getItemsList(ModelAndView model)
	{
		//Item list wll store all the items and displays using form..
		List<Item> listItem=irep.findAll();
		model.addObject("listItem",listItem);
		model.setViewName("ViewItems");
		return model;
	}
	@RequestMapping("/deleteItem")
	public String makeItemDelete(HttpServletRequest request)
	{
		int item_id=Integer.parseInt(request.getParameter("itemId"));
		//directly deletes an item from the table..
		irep.deleteById(item_id);
		return "redirect:/RetrieveItems";
	}
	@RequestMapping("/editItem")
	public ModelAndView getUpdateForm(HttpServletRequest request)
	{
		int item_id=Integer.parseInt(request.getParameter("itemId"));
		//gets the values of the item to be updated using getOne() method..
		Item item=irep.getOne(item_id);
		ModelAndView model=new ModelAndView("ItemUpdateForm","command",item);
		ditem=item;
		return model;
	}
	@RequestMapping("/getItems")
	public String getUpdatedItems(@ModelAttribute ("command") Item i1)
	{
		//getters and setters are used to update the item values.
		ditem.setItemName(i1.getItemName());
		ditem.setPrice(i1.getPrice());
		//after the update the values are saved..
		irep.save(ditem);
		return "redirect:/RetrieveItems";
	}
}
