package com.surabirestaurants.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.surabirestaurants.entity.User;
import com.surabirestaurants.repository.ItemRepository;
import com.surabirestaurants.repository.UserRepository;

@Controller
public class UserLoginController 
{
	@Autowired
	UserRepository urep;
	@Autowired
	ItemRepository irep;
	@RequestMapping("/userRegistrationLink")
	public ModelAndView getRegisterForm()
	{
		return new ModelAndView("UserRegisterForm","user1",new User());
	}
	
	@RequestMapping("/Register")
	public String getRegistration(@ModelAttribute ("user1") User user)
	{
		//saves the user details in the database.. 
		urep.save(user);
		return "redirect:/DisplayMenu";
	}
	
	@RequestMapping("/userLink")
	public ModelAndView getLoginForm()
	{
		return new ModelAndView("UserLoginForm","user2",new User());
	}
	
	@RequestMapping("/UserLoginValidation")
	public String checkingUserLogin(@ModelAttribute ("user2") User user)
	{
		//checks whether the user is valid or not from database..
		String uname=user.getUserName();
		String pwd=user.getPassword();
		List<User> ulist=urep.findAll();
		for(User u1:ulist)
		{
			if(u1.getUserName().equals(uname) && u1.getPassword().equals(pwd))
				return "redirect:/DisplayMenu";
		}
		return "redirect:/userLink";
	}
}
