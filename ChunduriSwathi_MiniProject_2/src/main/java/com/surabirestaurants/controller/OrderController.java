package com.surabirestaurants.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.surabirestaurants.entity.Item;
import com.surabirestaurants.entity.Order;
import com.surabirestaurants.entity.User;
import com.surabirestaurants.repository.ItemRepository;
import com.surabirestaurants.repository.OrderRepository;
@Controller
public class OrderController 
{
	@Autowired 
	OrderRepository orep;
	@Autowired 
	ItemRepository irep;
	
	//displays menu for the users to order the items
	@RequestMapping("/DisplayMenu")
	public ModelAndView getMenu(ModelAndView model)
	{
		List<Item> itemList=irep.findAll();
		model.addObject("itemList",itemList);
		model.setViewName("ViewMenu");
		return model;
	}
	
	@RequestMapping("/PlaceOrder")
	public ModelAndView getOrderForm(HttpServletRequest request,HttpSession session)
	{
		
		//LocalDate is used to get the todays date... 
		LocalDate date=LocalDate.now();
		int item_id=Integer.parseInt(request.getParameter("itemId"));
		Optional <Item> itemlist=irep.findById(item_id);
		Item i=itemlist.get();
		Order ord=new Order();
		ord.setItemid(i.getItemId());
		ord.setItemname(i.getItemName());
		ord.setPrice(i.getPrice());
		ord.setDate(date);
		ord.setQuantity(ord.getQuantity());
		ord.setUsername(ord.getUsername());
		return new ModelAndView("OrderForm","order",ord);
	}
	
	@RequestMapping("/SaveOrder")
	public String saveOrder(@ModelAttribute ("order") Order order)
	{
		//here the order values are stored in the database. 
		order.setTotalPrice(order.getPrice()*order.getQuantity());
		orep.save(order);
		return "Next";
	}
	
	@RequestMapping("/GenerateBill")
	public ModelAndView generateBill()
	{
		return new ModelAndView("UserBillForm","obj",new Order());
	}
	
	@RequestMapping("/Bill")
	public ModelAndView getBill(@ModelAttribute ("obj") Order order,HttpServletRequest req) 
	{
		//User user1=(User).SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //String username = User.getUserName();
		//generates the bill of the user..
		HttpSession ses=req.getSession();
		List<Order> user_name=orep.findAllByUsernameOrderByDateDesc(order.getUsername());
		String user= req.getParameter("username");
		LocalDate date=LocalDate.now();
		double grandTotal=orep.getBill(date,user);
		ses.setAttribute("GrandTotal",grandTotal);
		return new ModelAndView("Bill","bill",user_name);
	}
	
	@RequestMapping("/OrderHistory")
	public ModelAndView getform()
	{
		return new ModelAndView("UserOrderForm","obj1",new Order());
	}
	
	@RequestMapping("/SeeOrderHistory")
	public ModelAndView getOrderHistory(@ModelAttribute ("obj1") Order o)
	{
		//list of items bought will be shown in the order history...
		List<Order> user_name=orep.findAllByUsername(o.getUsername());
		return new ModelAndView("OrderHistory","orderlist",user_name);
	}
}

