package com.surabirestaurants.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class MainController
{
	@RequestMapping("/")
	public String getWelcomePage()
	{
		//Enters into the application
		return "Welcome";
	}
	@RequestMapping("/logout")
	public String signout(HttpServletRequest req)
	{
		//logout from the application and comes to login page...
		return "Welcome";
	}
}
