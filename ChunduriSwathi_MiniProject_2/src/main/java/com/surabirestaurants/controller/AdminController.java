package com.surabirestaurants.controller;

import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.surabirestaurants.entity.Admin;
import com.surabirestaurants.entity.Order;
import com.surabirestaurants.repository.AdminRepository;
import com.surabirestaurants.repository.OrderRepository;

@Controller
public class AdminController 
{
	@Autowired
	AdminRepository arep;
	@Autowired 
	OrderRepository orep;
	//Admin can only login to the application there is no registration for him..
	@RequestMapping("/adminLink")
	public ModelAndView getAdminform()
	{
		return new ModelAndView("AdminLoginForm","admin1",new Admin());
	}
	@RequestMapping("/CheckAdminLogin")
	public String chechAdmin(@ModelAttribute ("admin1") Admin admin)
	{
		//name and password will be taken and checks in the database..
		String aname=admin.getAdminName();
		String apwd=admin.getPassword();
		List<Admin> alist=arep.findAll();
		for(Admin a1:alist)
		{
			if(a1.getAdminName().equals(aname) && a1.getPassword().equals(apwd))
				//enters the application if the given credentials are valid..
				return "AdminMainPage";
		}
		return "redirect:/adminLink";

	}
	
	@RequestMapping("/AdminDashBoard")
	public String getDashboard()
	{
		return "AdminMainPage";
	}
	
	@RequestMapping("/SeeTodaysBill")
	public ModelAndView todaysBill(HttpServletRequest req)
	{
		//In the the total bill of the day will be calculated..
		HttpSession ses=req.getSession();
		LocalDate date=LocalDate.now();
		List<Order> orderlist=orep.findAllByDate(date);
		double todaysSale=orep.getTodaysbill(date);
		ses.setAttribute("bill", todaysSale);
		return new ModelAndView("TodaysBill","Billtoday",orderlist);
	}
	
	@RequestMapping("/SeeMonthBill")
	public String MonthlyBill(HttpServletRequest req)
	{
		//In the the monthly bill will be calculated..
		HttpSession ses=req.getSession();
		LocalDate date=LocalDate.now();
		int month=date.getMonthValue();
		double totalmonthsale=orep.getMonthSale(month);
		ses.setAttribute("MonthsBill",totalmonthsale);
		return "MonthlyBill";
	}
	
	@RequestMapping("/OrderbySpecificUser")
	public ModelAndView getform()
	{
		return new ModelAndView("OrderofSpecificUser","sorder",new Order());
	}
	
	@RequestMapping("/getOrdersOfSpecificUser")
	public ModelAndView  getOrders(@ModelAttribute ("sorder") Order ord)
	{
		List<Order> user_name=orep.findAllByUsername(ord.getUsername());
		return new ModelAndView("SpecificUserOrders","slist",user_name);
	}
}


