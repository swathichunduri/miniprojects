package com.surabirestaurants.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.surabirestaurants.entity.User;
import com.surabirestaurants.repository.ItemRepository;
import com.surabirestaurants.repository.UserRepository;

@Controller
public class UserCrudController 
{
	User duser;
	@Autowired
	UserRepository urep;
	@Autowired
	ItemRepository irep;
	
	@RequestMapping("/newUser")
	public ModelAndView getInsertForm()
	{
		return new ModelAndView("UserInsertForm","user3",new User());
	}
	
	@RequestMapping("/addUser")
	public String addingUser(@ModelAttribute ("user3") User user)
	{
		//Inserts users data into the database.. 
		urep.save(user);
		return "redirect:/RetrieveUsers";
	}
	@RequestMapping("/RetrieveUsers")
	public ModelAndView getUsers(ModelAndView model) 
	{
		//Listout all the users in the application..
		List<User> listUser=urep.findAll();
		model.addObject("listUser",listUser);
		model.setViewName("ViewUsers");
		return model;
	}
	
	@RequestMapping("/deleteUser")
	public String deleteUser(HttpServletRequest request)
	{
		int user_id=Integer.parseInt(request.getParameter("userId"));
		//deletes the user from the database..
		urep.deleteById(user_id);
		return "redirect:/RetrieveUsers";
	}
	
	@RequestMapping("/editUser")
	public ModelAndView getEditForm(HttpServletRequest request)
	{
		int user_id=Integer.parseInt(request.getParameter("userId"));
		User user=urep.getOne(user_id);
		//getOne() retrieves the present data related to specific user..
		ModelAndView model=new ModelAndView("UserUpdateForm","command",user);
		duser=user;
		return model;
	}
	
	@RequestMapping("/getUpdatedUser")
	public ModelAndView getUpdatedUsers(@ModelAttribute ("command") User u1)
	{
		//getters and setters update the data...
		duser.setUserName(u1.getUserName());
		duser.setPassword(u1.getPassword());
		duser.setEmailId(u1.getEmailId());
		duser.setPhoneNumber(u1.getPhoneNumber());
		//save() function again saves the updated values..
		urep.save(duser);
		return new ModelAndView("redirect:/RetrieveUsers");
	}

}
	
